![aglio](https://raw.github.com/danielgtaylor/aglio/master/images/aglio.png)

## Executable
Install aglio via NPM. You need Node.js installed and you may need to use `sudo` to install globally:

```bash
# instalar globalmente
sudo npm install -g https://bitbucket.org/bradoc/aglio.git

# mudar permissão do diretório "cache" de aglio-theme-olio
sudo chmod -R 777 /usr/local/lib/node_modules/aglio/node_modules/aglio-theme-olio/cache/ 
```

Then, start generating HTML.

```bash
# Default theme
aglio -i input.apib -o output.html
```